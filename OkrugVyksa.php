<?
    require("phpQuery.php");
    require("ModelNews.php");

    class OkrugVyksa{
        public function getPage($n = 1){
            // получение содержимого сайта
            if($n == 1){
                $url_page = 'http://okrug-wyksa.ru/';
            }
            else {
                $curpos = ($n-1)*7;
                $url_page = "http://okrug-wyksa.ru/?curPos={$curpos}";
            }
            
            $site = file_get_contents($url_page);
        
            $page = phpQuery::newDocument($site);
            
            $list_news = $page->find("ul.news_list");
        
            $news = $list_news->find("li");
                    
            foreach ($news as $nw){
                $model = new ModelNews();
                $pq = pq($nw);
                // заголовок
                $model->title = $pq->find("h3 > a")->text(); 
                // дата
                $data_find = str_replace('г.,', '', $pq->find("p.item_data > span")->text()); 
                $data_parse = explode(" ", $data_find); 
                $day = $data_parse[0];
                $month = $data_parse[1];
                $year = $data_parse[2]; 
                $time = $data_parse[4]; 
                $conv_data = $this->convertDate($day, $month, $year).", ".$time;
                $model->data = $conv_data; 
                // контент
                $model->content = $pq->find("p.item_content")->text();
                // картинка
                $model->image = $pq->find("p.item_content > a > img")->attr("src");
                // видео
                $model->video = $pq->find("div.item_video > iframe")->attr("src");      
                // тема - теги
                $model->tag = $pq->find("p.tags > a:last")->text(); 
                // детальная страница новости
                $model->source_link  = "http://okrug-wyksa.ru".$pq->find("h3 > a")->attr("href");
                        
                $detail = $this->DetailTex($model->source_link);     
                
                $model->description  = $detail['description'];
                $model->title_detail  = $detail['title_detail'];
                $model->images  = $detail['images'];
                $data[] = $model;
                
            }
            return $data;
        }
        function DetailTex($url_detail){
            $get_detail_page = file_get_contents($url_detail);  
            $dp = phpQuery::newDocument($get_detail_page);
            // заголовок
            $title = $dp->find(".l_cont > h1")->text();
            // текст
            $description = $dp->find(".nc_full_text")->text();
            // изображения
            $images = [];
            $images_detail = $dp->find(".nc_full_text p img");        
            foreach ($images_detail as $img){
                $pq = pq($img);
                $images[] = $pq->attr("src");
            }
            
            return [
                'title_detail' => $title,
                'description' => $description,
                'images' => $images
            
            ];
        }
        
        function convertDate($day, $month, $year){
            $months = [
                'Января' => 1,
                'Февраля' => 2,
                'Марта' => 3,
                'Апреля' => 4,
                'Мая' => 5,
                'Июня' => 6,
                'Июля' => 7,
                'Августа' => 8,
                'Сентября' => 9,
                'Октября' => 10,
                'Ноября' => 11,
                'Декабря' => 12,
            ];
            
            $dateTime = new DateTime();
            $dateTime->setTimezone(new DateTimeZone('Europe/Moscow'));
            $year = (int)trim($year);        
            $month = (int)$months[trim($month)];
            $day = (int)trim($day);
            $dateTime->setDate($year, $month, $day);
            return $dateTime->format('d.m.Y');
        }
    }
    
    $site = new OkrugVyksa;

    print_r($site->getPage(1));
    
?>